<?php 

defined('_JEXEC') or die;
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';
$w = htmlspecialchars($params->get('width'));
$h  = htmlspecialchars($params->get('height'));
$value  = htmlspecialchars($params->get('address'));
$type  = htmlspecialchars($params->get('type'));
$document = JFactory::getDocument();
$document->addStyleSheet('media/mod_mapper_choiser/css/geomap.css');
if($type == 1){
    $document->addScript('http://maps.google.com/maps/api/js?sensor=false');
    $document->addScript('media//mod_mapper_choiser/js/jquery.gomap-1.3.3.min.js');
}
elseif ($type == 2 ) {
     $document->addScript('http://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0');
    $document->addScript('media//mod_mapper_choiser/js/jquery.ui.bmap.min.js');
}
elseif ($type == 3 ) {
    $document->addStyleSheet('http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css');
    $document->addScript('http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js');
    $document->addScript('http://open.mapquestapi.com/sdk/leaflet/v1.s/mq-map.js?key=Fmjtd%7Cluu82q0y2l%2C85%3Do5-94ys0u');
    $document->addScript('http://open.mapquestapi.com/sdk/leaflet/v1.s/mq-geocoding.js?key=Fmjtd%7Cluu82q0y2l%2C85%3Do5-94ys0u');
}
$document->addScript('media/mod_mapper_choiser/js/map.js');
require JModuleHelper::getLayoutPath('mod_mapper_choiser');
?>